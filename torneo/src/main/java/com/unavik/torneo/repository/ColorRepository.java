package com.unavik.torneo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.unavik.torneo.entity.Color;

public interface ColorRepository extends JpaRepository<Color, Long> {

}

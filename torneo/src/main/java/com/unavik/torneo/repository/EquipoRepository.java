package com.unavik.torneo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.unavik.torneo.entity.Equipo;

public interface EquipoRepository extends JpaRepository<Equipo, Long> {

}

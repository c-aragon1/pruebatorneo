package com.unavik.torneo.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table
public class Equipo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    private String nombre;
    
    @ManyToOne
    @JoinColumn(name = "id_color_local", nullable = false)
    private Color colorLocal;
    
    @ManyToOne
    @JoinColumn(name = "id_color_visitante", nullable = false)
    private Color colorVisitante;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Color getColorLocal() {
        return colorLocal;
    }

    public void setColorLocal(Color colorLocal) {
        this.colorLocal = colorLocal;
    }

    public Color getColorVisitante() {
        return colorVisitante;
    }

    public void setColorVisitante(Color colorVisitante) {
        this.colorVisitante = colorVisitante;
    }
    
    
    
}

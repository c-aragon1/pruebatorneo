package com.unavik.torneo.exception;

public class DuplicateTeamException extends RuntimeException {
    
    /**
     * Arguments to replaced in message exception.
     */
    private Object[] args;
    
    private String message;
    
    public Object[] getArgs() {
        return args;
    }

    public DuplicateTeamException(String message, Object[] args) {
        super();
        this.args = args;
        this.message = message;
    }



    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setArgs(Object[] args) {
        this.args = args;
    }
    
    

}

package com.unavik.torneo.controller.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.unavik.torneo.controller.ColorController;
import com.unavik.torneo.entity.Color;
import com.unavik.torneo.service.ColorService;

@RestController
@RequestMapping(path = "color")
public class ColorControllerImpl implements ColorController {

    private ColorService colorService;
    
    @Autowired
    public ColorControllerImpl(ColorService colorService) {
        this.colorService = colorService;
    }
    
    
    @GetMapping(path = "")  
    @Override
    public List<Color> getAllColors() {
        return colorService.getAllColors();
    }    

}

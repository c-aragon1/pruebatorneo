package com.unavik.torneo.controller.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.unavik.torneo.controller.EquipoController;
import com.unavik.torneo.entity.Equipo;
import com.unavik.torneo.service.EquipoService;

@RestController
@RequestMapping(path = "equipo")
public class EquipoControllerImpl implements EquipoController {

    private EquipoService equipoService;
    
    @Autowired
    public EquipoControllerImpl(EquipoService equipoService) {
        this.equipoService = equipoService;
    }
    
    @PostMapping(path = "")
    @Override
    public Equipo saveTeam(@RequestBody Equipo equipo) {
        //TODO Hacer el cambio para recibir un DTO
       return equipoService.saveEquipo(equipo);
    }

}

package com.unavik.torneo.controller.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.unavik.torneo.controller.GameController;
import com.unavik.torneo.dto.GameDto;
import com.unavik.torneo.service.GameService;

@RestController
@RequestMapping(path = "game")
public class GameControllerImpl implements GameController {

    private GameService gameService;
    
    @Autowired
    public GameControllerImpl(GameService gameService) {
        this.gameService = gameService;
    }
    
    @PutMapping(path = "")
    @Override
    public List<GameDto> getGames() {
        return gameService.getGames();
    }

}

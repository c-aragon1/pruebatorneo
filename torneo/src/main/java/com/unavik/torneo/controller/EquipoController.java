package com.unavik.torneo.controller;

import com.unavik.torneo.entity.Equipo;

public interface EquipoController {

    Equipo saveTeam(Equipo equipo);
    
}

package com.unavik.torneo.controller;

import java.util.List;

import com.unavik.torneo.entity.Color;

public interface ColorController {
    
    List<Color> getAllColors();

}

package com.unavik.torneo.controller;

import java.util.List;

import com.unavik.torneo.dto.GameDto;

public interface GameController {

    List<GameDto> getGames();
    
}

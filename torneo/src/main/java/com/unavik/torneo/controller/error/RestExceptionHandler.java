package com.unavik.torneo.controller.error;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.unavik.torneo.dto.error.RestMessage;
import com.unavik.torneo.exception.DuplicateTeamException;
import com.unavik.torneo.exception.TeamIsFullyException;

@ControllerAdvice
public class RestExceptionHandler {

    /**
     * Message resources.
     */
    private final MessageSource messageSource;
    
    /**
     * Constructor.
     *
     * @param messageSource messageSource
     */
    @Autowired
    public RestExceptionHandler(final MessageSource messageSource) {
        this.messageSource = messageSource;
    }
    
    @ExceptionHandler(TeamIsFullyException.class)
    public ResponseEntity<RestMessage> handleIllegalArgument(final TeamIsFullyException ex, final Locale locale) {
        String errorMessage = messageSource.getMessage(ex.getMessage(), ex.getArgs(), locale);
        return new ResponseEntity<>(new RestMessage(errorMessage), HttpStatus.BAD_REQUEST);
    }
    
    @ExceptionHandler(DuplicateTeamException.class)
    public ResponseEntity<RestMessage> handleIllegalArgument(final DuplicateTeamException ex, final Locale locale) {
        String errorMessage = messageSource.getMessage(ex.getMessage(), ex.getArgs(), locale);
        return new ResponseEntity<>(new RestMessage(errorMessage), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    
}

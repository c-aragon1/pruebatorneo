package com.unavik.torneo.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.unavik.torneo.entity.Equipo;
import com.unavik.torneo.exception.DuplicateTeamException;
import com.unavik.torneo.exception.TeamIsFullyException;
import com.unavik.torneo.repository.EquipoRepository;
import com.unavik.torneo.service.EquipoService;

@Service
public class EquipoServiceImpl implements EquipoService {

    private EquipoRepository equipoRepository;
    
    public EquipoServiceImpl(EquipoRepository equipoRepository) {
        this.equipoRepository = equipoRepository;
    }
    
    @Override
    public Equipo saveEquipo(Equipo equipo) {
        //TODO Validar el equipo y hacer transformaciones de Dto a Entity y viceversa
        
        List<Equipo> equipos = equipoRepository.findAll();
        
        for(int i = 0; i < equipos.size(); i++) {
          //Esto es inválido porque compara objetos
            //if(equipos.get(i).getNombre() == equipo.getNombre()) { 
            if(equipos.get(i).getNombre().equals(equipo.getNombre())) {             
                throw new DuplicateTeamException("team.duplicate", null);
            }
        }
        
        /*for(Equipo equipoOnDb : equipos) {
            if(equipoOnDb.getNombre().equals(equipo.getNombre())) {
               throw new DuplicateTeamException("team.duplicate", null);
            }
        }*/
        
        if(equipoRepository.count() == 6) {
            throw new TeamIsFullyException("team.full", null);
        }
        
        
        
        return equipoRepository.save(equipo);
    }

}

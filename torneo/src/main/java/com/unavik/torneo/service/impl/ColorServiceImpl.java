package com.unavik.torneo.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unavik.torneo.entity.Color;
import com.unavik.torneo.repository.ColorRepository;
import com.unavik.torneo.service.ColorService;

@Service
public class ColorServiceImpl implements ColorService {

    private ColorRepository colorRepository;
    
    @Autowired
    public ColorServiceImpl(ColorRepository colorRepository) {
        this.colorRepository = colorRepository;
    }
    
    @Override
    public List<Color> getAllColors() {
        return colorRepository.findAll();
    }
    
}

package com.unavik.torneo.service;

import com.unavik.torneo.entity.Equipo;

public interface EquipoService {

    Equipo saveEquipo(Equipo equipo);
    
}

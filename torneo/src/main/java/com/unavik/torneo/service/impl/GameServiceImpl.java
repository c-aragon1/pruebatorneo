package com.unavik.torneo.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.unavik.torneo.dto.GameDto;
import com.unavik.torneo.entity.Equipo;
import com.unavik.torneo.exception.IncompleteTeamsException;
import com.unavik.torneo.repository.EquipoRepository;
import com.unavik.torneo.service.GameService;

@Service
public class GameServiceImpl implements GameService {

    private EquipoRepository equipoRepository;
    
    public GameServiceImpl(EquipoRepository equipoRepository) {
        this.equipoRepository = equipoRepository;
    }
    
    @Override
    public List<GameDto> getGames() {
        List<GameDto> games = new ArrayList<GameDto>();
        
        List<Equipo> equipos = equipoRepository.findAll();
        
        if(equipos.size() == 6) {
            for(int i = 0; i < equipos.size(); i++) {
                for(int j = 1 + i; j < equipos.size(); j++) {
                    if (i != j) {
                        GameDto game = new GameDto();
                        game.setDate(new Date());
                        game.setIdLocal(equipos.get(i).getId());
                        game.setLocal(equipos.get(i).getNombre());
                        game.setIdVisitante(equipos.get(j).getId());
                        game.setVisitante(equipos.get(j).getNombre());
                        games.add(game);
                    }
                }
            }
            return games;
        }
        throw new IncompleteTeamsException("teams.incomplete", null);
    }
}

package com.unavik.torneo.service;

import java.util.List;

import com.unavik.torneo.dto.GameDto;

public interface GameService {

    
    List<GameDto> getGames();
    
    
}
